import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:myapplication/models/car.dart';
import 'package:myapplication/run/library.dart';
import 'package:myapplication/widgets/singleCarCard.dart';

class Models extends StatelessWidget {
  List<Car> _cars = [];

  Car car;

  Models(this.car);

  @override
  Widget build(BuildContext context) {
    print("Car in models |" + car.getBrand() + "|");

    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.greenAccent,
          brightness: Brightness.dark),
      home: Scaffold(
          appBar: AppBar(
            title: Text(getTitle(car.getBrand())),
            leading: IconButton(
              icon: Icon(Icons.arrow_back_ios),
              onPressed: () => Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Library()),
              ),
            ),
          ),
          body: Stack(
            children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 60),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Hero(
                      tag: car.getImg(),
                      child: CircleAvatar(
                          backgroundImage:
                              AssetImage(getCarLogo(car.getBrand())),
                          radius: 60),
                    ),
                  )),
              FutureBuilder(
                  future: Hive.openBox(getModel(car.getBrand())),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.done) {
                      if (snapshot.hasError)
                        return Center(child: Text(snapshot.error));
                      else {
                        var cars = Hive.box(getModel(car.getBrand()));

                        if (cars.length == 0) {
                          if (getModel(car.getBrand()) == "audiCars") {
                            addAudiCars(cars);
                          } else if (getModel(car.getBrand()) ==
                              "MercedesCars") {
                            addMercedes(cars);
                          } else if (getModel(car.getBrand()) == "BMWCars") {
                            addBMW(cars);
                          } else {
                            print('unknown');
                          }
                        } else {
//                    Hive.deleteFromDisk();
                        }

                        if (_cars.length != 0) {
                          for (var i = 0; i < cars.length; i++) {
                            _cars.removeLast();
                          }
                        } else {
                          for (var i = 0; i < cars.length; i++) {
                            _cars.add(cars.getAt(i));
                          }
                        }

                        return Center(child: CarCard(_cars));
                      }
                    } else
                      return Center(child: CircularProgressIndicator());
                  })
            ],
          )),
    );
  }

  String getCarLogo(String carBrand) {
    String brandToReturn;
    if (carBrand == "Audi") {
      brandToReturn = "assets/audiLogo.png";
    } else if (carBrand == "Mercedes") {
      brandToReturn = "assets/mercedesLogo.png";
    } else if (carBrand == "BMW") {
      brandToReturn = "assets/bmwLogo.png";
    }
    return brandToReturn;
  }

  String getModel(String model) {
    String modelToReturn;

    switch (model) {
      case "Audi":
        modelToReturn = "audiCars";
        break;

      case "Mercedes":
        modelToReturn = "MercedesCars";
        break;

      case "BMW":
        modelToReturn = "BMWCars";
        break;
    }

    return modelToReturn;
  }

  String getTitle(String carModel) {
    String title;

    switch (carModel) {
      case "Audi":
        {
          title = "Audi";
          break;
        }

      case "BMW":
        {
          title = "BMW";
          break;
        }

      case "Mercedes":
        {
          title = "Mercedes";
          break;
        }

      default:
        {
          title = "undefined";
          break;
        }
    }

    return title;
  }

  void addAudiCars(var cars) {
    cars.add(new Car("Audi", "assets/audi/a1Sportback.jpg", "A1 Sportback","With its characteristic, smooth design, the second generation of the A1 picks up the lines of the first Audi quattro. \nThe interior is focused on the driver, the design has its own signature and the decoration can be personalized. The A1 Sportback therefore has the sportiest interior in its class. \nMoreover, it is available with infotainment and electronic assistance systems from much more expensive cars and the connectivity is also very good.",2));
    cars.add(new Car("Audi", "assets/audi/audiEtron.jpg", "Etron","The Audi e-tron is the first full-electrical model from the brand with the four rings. The sporty SUV combines the space and comfort of a typical luxury class automobile with a range suitable for everyday use, catapulting the driver into a new era with the electrical all-wheel drive. Forward-looking, innovative, and electric – the next step into the future.Combined electric power consumption in kWh/100 km (62.1 mi): 26.6 – 22.4 (WLTP); 24.3 – 21.0 (NEFZ);Combined CO2 emissions in g/km: 0Information on fuel/power consumption and CO2 emissions in ranges depending on the chosen equipment level of the car.",5));
    cars.add(new Car("Audi", "assets/audi/audiEtronSportback.jpg", "Etron Sportback","The Audi e-tron Sportback in conjunction with the S line exterior and virtual exterior mirrors achieves an outstanding drag coefficient value of just 0.25—even better than its Audi e-tron sister model. This is primarily due to the coupé body shape and the associated lower aerodynamic drag behind the car. The high separating edge of the Sportback minimizes swirl in the air flow in this area, which ultimately also benefits consumption. In the WLTP cycle, the SUV coupé has a range of up to 446 kilometers (277.1 mi) on a single battery charge. Roughly 10 kilometers (6.2 miles) of the increased range compared to the e-tron can be attributed to the aerodynamically more favorable body",3));
  }

  void addMercedes(var cars) {
    cars.add(new Car("Mercedes", "assets/mercedes/GLCClass.jpg", "GLC class","The GLC-Class is available in a five-door hatchback bodystyle with seating for five. Mercedes also offers a GLC Coupé, with four doors. In mainland Europe it is available with three diesel engines, three petrol engines, and a plug-in hybrid. Meanwhile in Britain only three diesels and one petrol AMG-model are offered.[6]Power comes from a choice of 2-liter 4-cylinder turbocharged petrol (same engine combined with the electric engine in the 350e hybrid) and 2.2-liter 4-cylinder turbo diesel engines in various power stages mated to a 9-speed (7-speed for the 350e hybrid) G-Tronic automatic transmission. 4MATIC all-wheel-drive is standard in some markets and optional in others. The GLC is 183.3 in (4,656 mm) long, 74.4 in (1,890 mm) wide and 64.5 in (1,638 mm) tall, with a track of 63.9 in (1,623 mm) in front and 63.7 in (1,618 mm) in the rear. Trunk volume is rated at 20.5 cu ft (580 L).In the German home market, the GLC 250 4MATI petrol version is joined by GLC 220d 4MATIC and GLC 250d 4MATIC diesel versions along with a plug-in hybrid version called the 350e. In North America, only the GLC 300, GLC 43 AMG and GLC 63 AMG are available.Daimler is operating a test fleet of fuel cell powered GLC models, dubbed GLC F-CELL, which will enter production in 2018.[7]",4));
    cars.add(new Car("Mercedes", "assets/mercedes/GClass.jpg", "G class","Mercedes-Benz G-class, sometimes referred to as G-Wagen (G - abbreviated from it. Geländewagen - [ɡəˈlɛːndəvaːɡn], “SUV”), is a series of full-size SUVs (off-road vehicles) manufactured in Austria by Magna Steyr [1] [2] (formerly Steyr-Daimler-Puch [3]) and sold under the brand name Mercedes-Benz. Available from 1979 to the present.The G-class was developed as a military vehicle at the suggestion of the Iranian Shah Mohammed Rez Pahlavi [1] [3] [4], which at that time was a shareholder of Mercedes-Benz. The civilian version of the car was introduced in 1979.Unlike other series of vehicles of the Mercedes-Benz trademark, G-class cars retain their unique appearance regardless of modification, whether factory or high-performance from the Mercedes-AMG division, for several decades [3].",5));
    cars.add(new Car("Mercedes", "assets/mercedes/EClass.jpg", "E class","The Mercedes-Benz E-Class is a range of executive cars manufactured by German automaker Mercedes-Benz in various engine and body configurations. Produced since 1953, the E-Class falls midrange in the Mercedes line-up, and has been marketed worldwide across five generations.Before 1993, the E in Mercedes-Benz nomenclature was a suffix following a vehicle's model number which stood for Einspritzmotor (German for fuel injection engine). It began to appear in the early 1960s, when that feature began to be utilized broadly in the maker's product line, and not just in its upper tier luxury and sporting models. By the launch of the facelifted W124 in 1993 fuel-injection was ubiquitous in Mercedes engines, and the E was adopted as a prefix (i.e., E 220) and the model line referred to officially as the E-Class (or E-Klasse). All generations of the E-Class have offered either rear-wheel drive or Mercedes' 4Matic four-wheel drive system.Historically, the E-Class is Mercedes-Benz' best-selling model, with more than 13 million sold by 2015.[1] The first E-Class series was originally available as four-door sedan, five-door station wagon, 2 door coupe and 2 door convertible. From 1997 to 2009, the equivalent coupe and convertible were sold under the Mercedes-Benz CLK-Class nameplate; which was actually based on the mechanical underpinnings of the smaller C-Class while borrowing the styling and some powertrains from the E-Class, a trend continued with the C207 E-Class coupe/convertible which was sold parallel to the W212 E-Class sedan/wagon. With the latest incarnation of the E-Class released for the 2017 model year, all body styles share the same W213 platform.[2]Due to the E-Class's size and durability, it has filled many market segments, from personal cars to frequently serving as taxis in European countries, as well special-purpose vehicles (e.g., police or ambulance modifications) from the factory.[",2));
  }

  void addBMW(var cars) {
    cars.add(new Car("BMW", "assets/bmw/series3.jpg", "Series 3","The BMW 3 Series is a compact executive car manufactured by the German automaker BMW since May 1975. It is the successor to the 02 Series and has been produced in seven different generations.The first generation of the 3 Series was only available as a 2-door sedan (saloon), however the model range has since expanded to include a 4-door sedan, 2-door convertible, 2-door coupé, 5-door station wagon, 5-door liftback and 3-door hatchback body styles. Since 2013, the coupé and convertible models have been marketed as the 4 Series, therefore the 3 Series range no longer includes these body styles.The 3 Series is BMW's best-selling model, accounting for around 30% of the BMW brand's annual total sales (excluding motorbikes).[1] The BMW 3 Series has won numerous awards throughout its history.The M version of the 3 series, M3, debuted with the E30 M3 in 1986",5));
    cars.add(new Car("BMW", "assets/bmw/series6.jpg", "Series 6","The BMW 6 Series is a range of grand tourers produced by BMW since 1976. It is the successor to the E9 Coupé and is currently in its fourth generation.The first generation BMW E24 6 Series was available solely as a two-door coupé and produced from 1976 to 1989. When the 6 Series nameplate was revived in 2004 for the second generation, the BMW E63/E64 6 Series, the coupé was joined by a convertible body style. The third generation F06/F12/F13 6 Series debuted in 2011 as a coupé and convertible, and added a four-door coupé body style (known as the Gran Coupé) in 2012. When the F06/F12/F13 6 Series ended production, the /coupé/convertible models have been shifted into the more upmarket BMW 8 Series (G15) nameplate. The fourth generation 6 Series, the G32 6 Series, debuted in mid-2017 and is offered only as a fastback body style to complement the BMW 5 Series (G30) sedan/wagon.[1][2]The first generation 6 Series was derived from the BMW E23 7 Series, and was powered by a range of naturally aspirated inline-six gasoline engines. Following generations have been powered by inline-four, V8, and V10 engines with both natural aspiration and turbocharging. Since 2008, diesel engines have been included in the 6 Series range, with four-wheel drive models on offer since 2012.[3]A BMW M6 high performance model has been produced for the first three generations of the 6 Series.",4));
    cars.add(new Car("BMW", "assets/bmw/x7.jpg", "X 7","The X7 is based on the Concept X7 iPerformance that was showcased during the 2017 Frankfurt Motor Show.The G07 features a self-levelling air suspension system, with a double-wishbone front suspension and multi-link rear suspension.[4] It can be raised or lowered by 40 mm (1.6 in) and will automatically lower the car by 20 mm (0.8 in) at speeds of over 138 km/h (86 mph).[5] The boot capacity is rated at 326 litres (11.5 cu ft), and 2,120 litres (75 cu ft) with the seats folded down.[6]All petrol and diesel models feature engine particulate filters and meet the Euro 6d-TEMP emissions standard.[7] The xDrive50i model is available outside the European markets, while the European markets will get the M50i models",3));
  }
}
