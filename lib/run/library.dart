import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:myapplication/models/car.dart';
import 'package:myapplication/run/models.dart';

import '../main.dart';

class Library extends StatelessWidget {
  List<Car> _cars = [];

  @override
  Widget build(BuildContext context) {
    _cars = <Car>[];

    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.greenAccent,
          brightness: Brightness.dark),
      home: Scaffold(
//        backgroundColor: Colors.deepPurpleAccent,
        appBar: AppBar(
          title: Text('Library'),
//          backgroundColor: Colors.deepPurple,

          leading: IconButton(
            icon: Icon(Icons.arrow_back_ios),
            onPressed: () => Navigator.of(context).push(
              MaterialPageRoute(
                settings: RouteSettings(name: 'Main'),
                builder: (context) => MyApp(),
              ),
            ),
          ),
        ),

        body: FutureBuilder(
            future: Hive.openBox("cars"),
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError) {
                  return Center(child: Text(snapshot.error));
                } else {
                  var cars = Hive.box("cars");

                  if (cars.length == 0) {
                    cars.add(new Car(
                        "Audi", "assets/audiLogo.png", "none", "none", 0));
                    cars.add(new Car("Mercedes", "assets/mercedesLogo.png",
                        "none", "none", 0));
                    cars.add(new Car(
                        "BMW", "assets/bmwLogo.png", "none", "none", 0));
                  } else {
//                    Hive.deleteFromDisk();
                  }

                  for (var i = 0; i < cars.length; i++) {
                    _cars.add(cars.getAt(i));
                  }

                  return CustomScrollView(
                    primary: false,
                    slivers: <Widget>[
                      SliverPadding(
                          padding: const EdgeInsets.all(40),
                          sliver: SliverGrid.count(
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10,
                            crossAxisCount: 2,
                            children: getCars(context),
                          ))
                    ],
                  );
                }
              } else
                return Center(child: CircularProgressIndicator());
            }),

        bottomNavigationBar: BottomAppBar(
          child: Row(),
//          color: Colors.deepPurple,
        ),
      ),
    );
  }

  List<Widget> getCars(BuildContext context) {
    List<Widget> childs = [];

    _cars
        .map((car) => childs.add(new GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Models(car),
                  ),
                );
              },
              child: Column(
                children: <Widget>[
                  Hero(
                    tag: car.getImg(),
                    child: CircleAvatar(
                        backgroundImage: AssetImage(car.getImg()), radius: 60),
                  ),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(
                        car.getBrand(),
                        style: TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ))
                ],
              ),
            )))
        .toList();

    return childs;
  }
}
