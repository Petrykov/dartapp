import 'package:flutter/material.dart';
import 'package:myapplication/models/car.dart';

import 'models.dart';

class SingleCar extends StatelessWidget {

  Car _car;

  SingleCar(Car car) {
    _car = car;
  }

  @override
  Widget build(BuildContext context) {
    print(_car.getBrand());

    return MaterialApp(
        theme: ThemeData(
            primarySwatch: Colors.deepPurple,
            accentColor: Colors.greenAccent,
            brightness: Brightness.dark),
        home: Scaffold(
//        backgroundColor: Colors.deepPurpleAccent,
            appBar: AppBar(
              title: Text(_car.getBrand()),
              leading: IconButton(
                icon: Icon(Icons.arrow_back_ios),
                onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => Models(_car)),
                ),
              ),
            ),
            body: Column(
              children: <Widget>[
                Align(
                    alignment: Alignment.topCenter,
                    child: Container(
                      margin: EdgeInsets.only(top: 50),
                      height: MediaQuery.of(context).size.height - 400,
                      width: MediaQuery.of(context).size.width - 100,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20.0),
                          color: Colors.black12),
                      child: Column(
                        children: <Widget>[
                          Container(
                              width: MediaQuery.of(context).size.width,
                              height: 200,
                              child: Container(
                                  decoration: BoxDecoration(
                                    boxShadow: <BoxShadow>[
                                      BoxShadow(
                                          color: Colors.black54,
                                          blurRadius: 30.0,
                                          offset: Offset(0.0, 0.20))
                                    ],
                                  ),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(20.0),
                                    child: Image.asset(_car.getImg(),
                                        fit: BoxFit.fill),
                                  ))),
                          Container(
//                    color: Colors.blueGrey,
                              child: Padding(
                                  padding: EdgeInsets.only(
                                      top: 25, left: 25, right: 25),
                                  child: Column(
                                    children: <Widget>[
                                      Align(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                            _car.getBrand() +
                                                " " +
                                                _car.getModel(),
                                            style: TextStyle(
                                                letterSpacing: 1,
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold)),
                                      ),
                                      Container(
                                          margin: EdgeInsets.only(top: 15),
                                          height: 170,
                                          child: SingleChildScrollView(
                                              child: Text(
                                            _car.getDescription(),
                                            style: TextStyle(letterSpacing: 1),
                                          ))),
                                    ],
                                  ))),
                          Padding(
                              padding: EdgeInsets.only(top: 15, left: 25),
                              child: stars(_car.getRating()))
                        ],
                      ),
                    )),
                Container(
                  margin: EdgeInsets.only(top: 40),
                  child: Hero(
                    tag: _car.getImg(),
                    child: CircleAvatar(
                        backgroundImage:
                            AssetImage(getCarLogo(_car.getBrand())),
                        radius: 60),
                  ),
                )
              ],
            )));
  }

  Widget stars(int stars) {
    List<Widget> toReturn = new List<Widget>();
    int maxRating = 5;

    if (stars <= maxRating) {
      for (var i = 0; i < stars; i++) {
        toReturn.add(new Icon(Icons.star_border,
            size: 24.0, color: Colors.yellowAccent));
      }
    }

    int starsLeft = maxRating - stars;

    for (var i = 0; i < starsLeft; i++) {
      toReturn.add(new Icon(Icons.star_border, size: 24.0));
    }

    return new Row(
        mainAxisAlignment: MainAxisAlignment.start, children: toReturn);
  }

  String getCarLogo(String carBrand) {
    String brandToReturn;
    if (carBrand == "Audi") {
      brandToReturn = "assets/audiLogo.png";
    } else if (carBrand == "Mercedes") {
      brandToReturn = "assets/mercedesLogo.png";
    } else if (carBrand == "BMW") {
      brandToReturn = "assets/bmwLogo.png";
    }
    return brandToReturn;
  }

  String getBrand(String brand) {
    String logoImage;

    if (brand == "Audi") {
      logoImage = "assets/audiLogo.png";
    } else if (brand == "Mercedes") {
      logoImage = "assets/mercedesLogo.png";
    } else if (brand == "BMW") {
      logoImage = "assets/bmwLogo.png";
    }

    return logoImage;
  }
}
