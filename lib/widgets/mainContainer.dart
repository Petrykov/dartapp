import 'package:flutter/material.dart';
import 'package:diagonal/diagonal.dart';

class MainContainer extends StatelessWidget {

  Position _position;
  EdgeInsets _edgeInsetsFirst;
  int _alwaysStoppedAnimationFirst, _alwaysStoppedAnimationSecond;
  EdgeInsets _edgeInsetsSecond;
  String _text;
  double _blurRadius;
  MaterialPageRoute _materialPageRoute;

  MainContainer(Position position, EdgeInsets edgeInsetsFirst, int alwaysStoppedAnimationFirst, int alwaysStoppedAnimationSecond,
                  EdgeInsets edgeInsetsSecond, String text, double blurRadius, MaterialPageRoute materialPageRoute) {

    _position = position;
    _edgeInsetsFirst = edgeInsetsFirst;
    _alwaysStoppedAnimationFirst = alwaysStoppedAnimationFirst;
    _alwaysStoppedAnimationSecond = alwaysStoppedAnimationSecond;
    _edgeInsetsSecond = edgeInsetsSecond;
    _text = text;
    _blurRadius = blurRadius;
    _materialPageRoute = materialPageRoute;
  }

  @override
  Widget build(BuildContext context) {

    return Expanded(
        child: Align(
            alignment: FractionalOffset.bottomCenter,
            child: Diagonal(
                position: _position,
                clipHeight: 80.0,
                child: Container(
                    margin: _edgeInsetsFirst,
                    height: 200,
                    alignment: Alignment.center,
                    child: ButtonBar(
                      alignment: MainAxisAlignment.center,
                      children: <Widget>[
                        FlatButton(
                          child: RotationTransition(
                              turns: new AlwaysStoppedAnimation(
                                  _alwaysStoppedAnimationFirst /
                                      _alwaysStoppedAnimationSecond),
                              child: Center(
                                  child: Padding(
                                padding: _edgeInsetsSecond,
                                child: Text(
                                  _text,
                                  style: TextStyle(
                                      letterSpacing: 3,
                                      fontSize: 35,
                                      fontFamily: 'BlackBoard'),
                                ),
                              ))),
                          onPressed: () => Navigator.of(context).push(
                            _materialPageRoute
                          ),
                        ),
                      ],
                    ),
                    decoration: new BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.blueGrey,
                          blurRadius: _blurRadius,
                          // has the effect of softening the shadow
                          // has the effect of extending the shadow
                          offset: Offset(
                            15.0, // horizontal, move right 10
                            10.0, // vertical, move down 10
                          ),
                        )
                      ],
                    )))));
  }
}
