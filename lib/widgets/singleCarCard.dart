import 'package:flutter/material.dart';
import 'package:myapplication/models/car.dart';
import 'package:myapplication/run/singleCar.dart';


class CarCard extends StatelessWidget {

  List <Car> _cars = [];

  CarCard(List <Car> cars){
      _cars = cars;
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      margin: EdgeInsets.only(bottom: 60.0),
      height: 220.0,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: _cars.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(10.0)),
              elevation: 5,
              margin: EdgeInsets.all(10),
              semanticContainer: true,
              child: Stack(
                children: <Widget>[
                  new GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => SingleCar(_cars[index])),
                      );
                    },
                    child:Container(
                      child: Center(
                        child: Padding(
                          padding: EdgeInsets.only(top: 140.0),
                          child: Text(
                              '${_cars[index].getModel()}',
                              style: TextStyle(fontSize: 20)),
                        ),
                      ),
                      height: 220.0,
                      width: MediaQuery.of(context)
                          .size
                          .width -
                          100.0,
                      decoration: BoxDecoration(
                          borderRadius:
                          BorderRadius.circular(5),
                          image: DecorationImage(
                              colorFilter:
                              new ColorFilter.mode(
                                  Colors.black
                                      .withOpacity(0.4),
                                  BlendMode.dstATop),
                              image: AssetImage(
                                  '${_cars[index].getImg()}'),
                              fit: BoxFit.fill)),
                    )
                  )

                ],
              ),
            );
          }),
    );
  }

}
