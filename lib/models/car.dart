import 'package:hive/hive.dart';

part 'car.g.dart';

@HiveType(typeId: 0)
class Car {
  @HiveField(0)
  String _brand = '';
  @HiveField(1)
  String _img = '';
  @HiveField(2)
  String _model = '';
  @HiveField(3)
  String _description = '';
  @HiveField(4)
  int _rating;

  Car(this._brand,this._img, this._model, this._description, this._rating);

  String getBrand() {
    return _brand;
  }

  String getImg() {
    return _img;
  }

  String getModel() {
    return _model;
  }

  String getDescription(){
    return _description;
  }

  int getRating(){
    return _rating;
  }

}
