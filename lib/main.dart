import 'package:flutter/material.dart';
import 'package:myapplication/models/car.dart';
import 'package:myapplication/widgets/mainContainer.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:myapplication/run/library.dart';
import 'package:diagonal/diagonal.dart';
import 'package:path_provider/path_provider.dart' as pathProvider;
import 'package:hive/hive.dart';

// alternative for 'void main'
// main() => runApp(MyApp());
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  var appDocumentDirectory =
      await pathProvider.getApplicationDocumentsDirectory();
  Hive.init(appDocumentDirectory.path);
  Hive.registerAdapter(CarAdapter());

  runApp(new MaterialApp(
    debugShowCheckedModeBanner: false,
    home: new MyApp(),
  ));
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return SplashScreen(
      seconds: 1,
      backgroundColor: Colors.white,
      image: Image.asset('assets/logoApp.png'),
      photoSize: 170,
      loaderColor: Colors.blueAccent,
      navigateAfterSeconds: MainScreen(),
    );
  }
}

class MainScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.greenAccent,
          brightness: Brightness.dark),
      home: Scaffold(
        appBar: AppBar(
          title: Text('West Coast Customs'),
        ),
        body: Column(
          children: <Widget>[
            ClipPath(
                clipper: MyClipper(),
                child: Container(
                  height: 215,
                  width: double.infinity,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [Colors.grey, Colors.blueGrey]),
                      image: DecorationImage(
                        image: AssetImage(
                          "assets/westCoast.png",
                        ),
                        fit: BoxFit.none,
                        alignment: Alignment.topCenter,
                      )),
                )),
            MainContainer(
                Position.TOP_LEFT,
                const EdgeInsets.only(bottom: 90.0),
                10,
                360,
                const EdgeInsets.only(top: 40),
                "Library",
                50.0,
                MaterialPageRoute(builder: (context) => Library())),
            MainContainer(
                Position.TOP_RIGHT,
                const EdgeInsets.only(top: 100.0),
                350,
                360,
                const EdgeInsets.only(bottom: 50),
                "Favourites",
                65.0,
                null),
          ],
        ),
      ),
    );
  }
}

class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    var path = Path();
    path.lineTo(0, size.height - 80);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 80);
    path.lineTo(size.width, 0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}
